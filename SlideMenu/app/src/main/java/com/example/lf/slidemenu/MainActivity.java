package com.example.lf.slidemenu;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lf.slidemenu.customview.MyLinearLayout;
import com.example.lf.slidemenu.customview.MySlideMenu;
import com.example.lf.slidemenu.utils.Constant;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ListView menuListView, mainListView;
    private ImageView igIcon;
    private MySlideMenu slideMenu;
    private MyLinearLayout layou;

    private static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }


    private void initView() {
        mainListView = (ListView) findViewById(R.id.main_listview);
        menuListView = (ListView) findViewById(R.id.menu_listview);
        slideMenu = (MySlideMenu) findViewById(R.id.activity_main);
        igIcon = (ImageView) findViewById(R.id.iv_head);
        layou = (MyLinearLayout) findViewById(R.id.my_layout);
    }

    private void initData() {

        menuListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Constant.sCheeseStrings) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                textView.setTextColor(Color.WHITE);
                return textView;
            }
        });

        mainListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Constant.NAMES) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = convertView == null ? super.getView(position, convertView, parent) : convertView;
                //先缩小view
                ViewHelper.setScaleX(view, 0.5f);
                ViewHelper.setScaleY(view, 0.5f);
                //以属性动画放大
                ViewPropertyAnimator.animate(view).scaleX(1).setDuration(350).start();
                ViewPropertyAnimator.animate(view).scaleY(1).setDuration(350).start();
                return view;
            }
        });

        slideMenu.setListener(new MySlideMenu.OnDragStateChangeListener() {
            @Override
            public void onOpen() {
                menuListView.smoothScrollToPosition(new Random().nextInt(menuListView.getCount()));
            }

            @Override
            public void onClose() {
                //头像左右抖动
                ViewPropertyAnimator.animate(igIcon).translationXBy(15)
                        .setInterpolator(new CycleInterpolator(4))
                        .setDuration(500)
                        .start();
            }

            @Override
            public void onDraging(float fraction) {
                //头像的隐藏
                ViewHelper.setAlpha(igIcon, 1 - fraction);
            }
        });
        layou.setSlideMenu(slideMenu);

    }
}
